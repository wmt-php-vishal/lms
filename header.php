<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <title>LMS</title>
  </head>
  <body>

    <header>
      <nav class="navbar navbar-expand-sm bg-dark navbar-dark ">

          <nav class="nav-brand text-white" >Library Management System</nav>

          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="collapsibleNavbar">
          <ul class="navbar-nav ml-auto" >
            <li class="nav-item"><a href="/LMS/index.php" class="nav-link">Home</a></li>
            <li class="nav-item"><a href="/LMS/author/authordisplay.php" class="nav-link">Author</a></li>
            <li class="nav-item"><a href="/LMS/book/bookdisplay.php" class="nav-link">Book</a></li>
            
  
            <li class="nav-item"><a href="/LMS/logout.php" class="nav-link">Logout</a></li>
          </ul>

          </div>
        </nav>
      </header>


  </body>
</html>
