<?php


session_start();
include "/opt/lampp/htdocs/LMS/header.php";
include "/opt/lampp/htdocs/LMS/connection.php";

$sid = $_GET['sid'];



$selectq = mysqli_query($connection, "SELECT * FROM books WHERE book_id=$sid");
$row = mysqli_fetch_array($selectq);

if($_GET['sid'] == ""){
    header("location:bookdisplay.php");
}

$selectauthor = mysqli_query($connection, "SELECT author_id, CONCAT(author_fname , ' ',  author_lname)
AS fullname FROM author WHERE author_id= {$row['author_id']}");
$authordata = mysqli_fetch_array($selectauthor);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <title>Document</title>
</head>
<body>

<table border="1" class="text-center  px-5 m-4">
<tr>
<th>Book Id</th>

<td><?= $row['book_id']?></td>
</tr>

<tr>
<th>Title</th>
<td><?= $row['title']?></td>
</tr>

<tr>
<th>Pages</th>
<td><?= $row['pages']?></td>
</tr>

<tr>
<th>Description</th>
<td><?= $row['book_description']?></td>
</tr>

 <tr>
<th>Author Name</th>
<td><?= $authordata['fullname']?></td>
</tr> 

<tr>
<th>Author Id </th>
<td><?= $row['author_id']?></td>
</tr>

</table>

    
</body>
</html>