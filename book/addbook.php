<?php

session_start();
include "/opt/lampp/htdocs/LMS/header.php";
include "/opt/lampp/htdocs/LMS/connection.php";
include "/opt/lampp/htdocs/LMS/loginSessionValid.php";

$selectauthor = mysqli_query($connection, "SELECT author_id, CONCAT(author_fname , ' ',  author_lname) AS fullname FROM author");
//$row = mysqli_fetch_array($selectauthor);

$titleerr = $pageserr = $descriptionerr = $authoriderr = "";

if ($_SERVER['REQUEST_METHOD'] == "POST") {

    if (empty($_POST['title'])) {
        $titleerr = "Title is required";
    } else {
        $title = test_input($_POST['title']);
    }

    if (empty($_POST['pages'])) {
        $pageserr = "Pages is required";
    } else {
        $pages = test_input($_POST['pages']);
        if (!preg_match("/^[0-9 ]*$/", $pageserr)) {
            $pageserr = "Only numbers are allowed in pages";
        }
    }

    if (empty($_POST['description'])) {
        $descriptionerr = "Description is required";
    }

    if (empty($_POST['authorid'])) {
        $authoriderr = "Author details are required";
    }

    $title = test_input($_POST['title']);
    $pages = test_input($_POST['pages']);
    $description = test_input($_POST['description']);
    $authorid = test_input($_POST['authorid']);

    if ($titleerr || $pageserr || $descriptionerr || $authoriderr) {

    } else {
        $insertbook = mysqli_query($connection, "INSERT INTO books(title, pages,
        book_description, author_id) VALUES ('" . $title . "','" . $pages . "','" . $description . "','" . $authorid . "')") or die(mysqli_error($connection));

        if ($insertbook) {
            echo "Book inserted in database";
        } else {
            echo "insertion failed";
        }

    }

}

function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<form method="POST">



<!-- <div class="form-group row mt-5">
    <label for="" class="col-sm-2 col-form-label">Book Id</label>
    <div class="col-sm-4">
    <label for="" class=" col-form-label">Book Id</label>
    </div>
  </div> -->


  <div class="form-group row mt-5">
    <label for="" class="col-sm-2 col-form-label">Title</label>
    <div class="col-sm-4">
      <input type="text" class="form-control" name="title"  placeholder="Title"><?=$titleerr?>
    </div>
  </div>
  <div class="form-group row">
    <label for="" class="col-sm-2 col-form-label">Pages</label>
    <div class="col-sm-4">
      <input type="text" class="form-control" name="pages"  placeholder="Pages"><?=$pageserr?>
    </div>
  </div>



  <div class="form-group row">
    <label for="" class="col-sm-2 col-form-label">Author's Name</label>
    <div class="col-sm-4">
    <select name="authorid" class="form-control">
  <option value="#">Select Author  </option>

  <?php while ($row = mysqli_fetch_array($selectauthor)) {?>

    <option  value="<?=$row['author_id']?>"> <?=$row['fullname']?> </option>


<?php }?>

</select>
<?=$authoriderr?>
</div>

  </div>

<div class="form-group row">
    <label for="" class="col-sm-2 col-form-label">Description</label>
    <div class="col-sm-4">
      <!-- <input type="text" class="form-control" name="description"  placeholder="Description"><?=$descriptionerr?> -->
      <textarea name="description" class="form-control" id="" cols="30" rows="10"></textarea>
    </div>
  </div>


  <div class="form-group row">
    <div class="col-sm-10">
      <button type="submit" class="btn btn-primary">Add Book</button>
    </div>
  </div>
</form>


</body>
</html>