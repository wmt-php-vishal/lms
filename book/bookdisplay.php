<?php

session_start();
include "/opt/lampp/htdocs/LMS/header.php";
include "/opt/lampp/htdocs/LMS/connection.php";
include "/opt/lampp/htdocs/LMS/loginSessionValid.php";


$selectq = mysqli_query($connection, "SELECT * FROM books");





?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>

    <title>Document</title>
</head>
<body>

<h2 class="text-center mt-5">Book Display</h2>
<table class="table mt-3 ">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Book id</th>
      <th scope="col">Title</th>
      <th scope="col">Pages</th>
      <th scope="col">Description</th>
      <th scope="col">Author id</th>
      <th scope="col">Author Name</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
  <?php 
  
  
  while ($row = mysqli_fetch_array($selectq)) {

    $selectauthor = mysqli_query($connection, "SELECT author_id, CONCAT(author_fname , ' ',  author_lname)
AS fullname FROM author WHERE author_id= {$row['author_id']}");
$authordata = mysqli_fetch_array($selectauthor);

    ?>
    <tr>
      <th scope="row"> <?=$row['book_id']?> </th>
      <td>
      <?php
     echo " <a href='showbook.php?sid={$row['book_id']}' class='text-decoration-none'>"; 
     echo $row['title'];
     
     
      ?>
      </a></td>
      <td> <?=$row['pages']?> </td>
      <td> <?=$row['book_description']?> </td>
      <td> <?=$row['author_id']?> </td>
      <td> <?=$authordata['fullname']?> </td>
      <td>
      <?php
      echo "<a href='editbook.php?eid={$row['book_id']}'><i class='fas fa-edit mr-3'></i></a>  
      <a href='bookdelete.php?did={$row['book_id']}'><i class='fas fa-trash-alt'></i></a>";
      ?> 
      </td>


    </tr>

    <?php } ?>


    
  </tbody>
</table>

    <div class="d-flex justify-content-center mt-4">
    <button class="btn btn-lg btn-dark "><a class="text-decoration-none" href="addbook.php">Add New Book</a></button>
    </div>
</body>
</html>