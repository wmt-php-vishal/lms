<?php

session_start();

include "/opt/lampp/htdocs/LMS/connection.php";
include "/opt/lampp/htdocs/LMS/header.php";
include "/opt/lampp/htdocs/LMS/loginSessionValid.php";

$title = $pages = $authorid = $description = "";
$titleerr = $pageserr = $authoriderr = $descriptionerr = "";

if($_GET['eid'] == ""){
    echo header("location:bookdisplay.php");
    die();
}

$eid = $_GET['eid'];
$selectauthor = mysqli_query($connection, "SELECT author_id, CONCAT(author_fname , ' ',  author_lname)
AS fullname FROM author");

$selectq = mysqli_query($connection, "SELECT book_id, title, pages, book_description,  author_id FROM books
WHERE book_id={$eid}");
$data = mysqli_fetch_array($selectq);


if($_SERVER['REQUEST_METHOD'] == 'POST')
{


    if (empty($_POST['title'])) {
        $titleerr = "Title is required";
    } else {
        $title = test_input($_POST['title']);
    }

    if (empty($_POST['pages'])) {
        $pageserr = "Pages is required";
    } else {
        $pages = test_input($_POST['pages']);
        if (!preg_match("/^[0-9 ]+$/", $pages)) {
            $pageserr = "Only numbers are allowed in pages";
        }
    }

    if (empty($_POST['description'])) {
        $descriptionerr = "Description is required";
    }

    if (empty($_POST['authorid'])) {
        $authoriderr = "Author details are required";
    }

    $title = test_input($_POST['title']);
    $pages = test_input($_POST['pages']);
    $authorid = test_input($_POST['authorid']);
    $description = test_input($_POST['description']);

    $updateq = mysqli_query($connection, "UPDATE books SET title='{$title}' ,pages='{$pages}' ,
           book_description='{$description}' ,author_id='{$authorid}' WHERE book_id=$eid") or die(mysqli_error($connection));

    if($updateq){
        header("location:bookdisplay.php");

    }
    else{
        echo "book not added";
    }
}

function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>

    <title>Document</title>
</head>
<body>


<form method="POST">

  <div class="form-group row mt-5">
    <label for="" class="col-sm-2 col-form-label">Title</label>
    <div class="col-sm-4">
      <input type="text" class="form-control" name="title" value="<?=$data['title']?>"  ><?=$titleerr?>
    </div>
  </div>
  <div class="form-group row">
    <label for="" class="col-sm-2 col-form-label">Pages</label>
    <div class="col-sm-4">
      <input type="text" class="form-control" name="pages" value="<?=$data['pages']?> " ><?=$pageserr?>
    </div>
  </div>



  <div class="form-group row">
    <label for="" class="col-sm-2 col-form-label">Author's Name</label>
    <div class="col-sm-4">
    <select name="authorid" class="form-control">
  <option value="#">Select Author  </option>

  <?php while ($row = mysqli_fetch_array($selectauthor)) {?>

    <option  value="<?=$row['author_id']?>" <?=($row['author_id'] == $data['author_id']) ? "selected" : ""?>> <?=$row['fullname']?> </option>


<?php }?>

</select>
<?=$authoriderr?>
</div>

  </div>

  <div class="form-group row mt-5">
    <label for="" class="col-sm-2 col-form-label">Description</label>
    <div class="col-sm-4">
      <input type="text" class="form-control" name="description" value="<?php echo $data['book_description']; ?>"  ><?=$descriptionerr?>
    </div>
  </div>

  <div class="form-group row mt-5">
    <div class="col-sm-4">
      <input type="submit" class="btn-lg btn-dark" name="button" value="Update Book" >
    </div>
  </div>



</form>

</body>
</html>