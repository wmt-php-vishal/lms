<?php

session_start();

include "/opt/lampp/htdocs/LMS/header.php";
include "/opt/lampp/htdocs/LMS/connection.php";
include "/opt/lampp/htdocs/LMS/loginSessionValid.php";

error_reporting(-1);
$adisplay = mysqli_query($connection, "SELECT author_id, CONCAT(author_fname, ' ' ,  author_lname) fullname, dob ,gender , mobile ,author_description FROM  author");

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <script src="https://kit.fontawesome.com/a076d05399.js"></script>

</head>
<body>
<br>
<h3 class="text-center">Author Display</h3>
<br>
<table class="table table-bordered">
<tbody>

    <thead>
      <tr>
      <td>Author Id</td>
<td>Full Name</td>
<td>DOB</td>
<td>Gender</td>
<td>Mobile</td>
<td>Description</td>
<td>Actions</td>
      </tr>
    </thead>
    <body>
        <?php
while ($row = mysqli_fetch_array($adisplay)) {
    ?>
      <tr>
        <td><?=$row['author_id']?></td>
        <td> <?php 
        echo "<a href='showauthor.php?sid={$row['author_id']}'> ";
        echo "{$row['fullname']}</a></td>";
        ?>
        <td><?=$row['dob']?></td>
        <td><?=$row['gender']?></td>

        <td><?=$row['mobile']?></td>
        <td><?=$row['author_description']?></td>
        <td>
            <?php echo " <a href='edit.php?eid={$row['author_id']}'>
                <i class='fas fa-edit'></i>
                </a>
                 <a href='delete.php?did={$row['author_id']}'><i class='fas fa-trash-alt'></i></a> ";
    ?>
        </td>



      </tr>
        <?php }?>
    </tbody>
  </table>
  <div class="d-flex justify-content-center mt-5">
  <button class='btn btn-dark btn-lg '> <a href='addauthor.php'>Add New Author</a></button>
  </div>



</body>
</html>