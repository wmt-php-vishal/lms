<?php

session_start();
include "/opt/lampp/htdocs/LMS/connection.php";
include "/opt/lampp/htdocs/LMS/header.php";
include "/opt/lampp/htdocs/LMS/loginSessionValid.php";



$fname = $lname = $dob = $gender = $mobile = $description = "";

$fnameerr = $lnameerr = $doberr = $gendererr = $mobileerr = $descriptionerr = "";

$eid = $_GET['eid'];

if($_GET['eid'] == "")
{
    header("location:authordisplay.php");
}


$selectq = mysqli_query($connection, "SELECT * FROM author WHERE author_id = {$eid}");

$row = mysqli_fetch_array($selectq);


if($_SERVER['REQUEST_METHOD'] == "POST"){


    if (empty($_POST['fname'])) {
        $fnameerr = "First name is required";
    } else {
        $fname = test_input($_POST['fname']);
        if (!preg_match("/^[a-zA-Z ]*$/", $fname)) {
            $fnameerr = "Only whitespace and letters allowed in fname";
        } else {
            $fname = test_input($_POST['fname']);

        }
    }

    if (empty($_POST['lname'])) {
        $lnameerr = "Last name is required";
    } else {
        $lname = test_input($_POST['lname']);
        if (!preg_match("/^[a-zA-Z ]*$/", $lname)) {
            $lnameerr = "Only whitespace and letters allowed in lname";
        }
    }

    if (empty($_POST['gender'])) {
        $gendererr = "Gender is required";
    }

    if (empty($_POST['mobile'])) {
        $mobileerr = "Mobile Number is required";
    } 


    $fname = test_input($_POST['fname']);
    $lname = test_input($_POST['lname']);
    $dob = test_input($_POST['dob']);
    $gender = test_input($_POST['gender']);
    $mobile = test_input($_POST['mobile']);
    $description = test_input($_POST['description']);



if ($fnameerr || $lnameerr || $doberr || $gendererr || $mobileerr || $descriptionerr)
{
    // echo "<script>alert ('Kindly enter valid data to proceed further'); </script>";

}
else{

    $updateq = mysqli_query($connection, "UPDATE author SET author_fname='{$fname}',
    author_lname='{$lname}', dob='{$dob}', gender='{$gender}', mobile='{$mobile}', author_description='{$description}' 
    WHERE author_id={$eid} ") or die(mysqli_error($connection));
    
    
    if($updateq){
        header("location:authordisplay.php");
        $_SESSION['updatemsg'] = "Data updated Successfully";
    }
    else{
        echo "updation failed";
    }
    


}



}



function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Update Author</title>
</head>
<body>

<form method="POST" class="mt-3 ">
    <h3>Update Author's Information</h3><br>
    <table  class="d-flex justify-content-">
        <?php echo ""; ?>

        <tr>
            <td>Author Id</td>
            <td><label for="id"><?=$row['author_id']?></label></td>
        </tr>

        <tr>
            <td>First Name</td>
            <td><input type="text" name="fname" value="<?php echo $row['author_fname']; ?>"><?= $fnameerr ?></td>
        </tr>


        <tr>
            <td>Last Name</td>
            <td><input type="text" name="lname" value="<?php echo $row['author_lname']; ?>" ><?= $lnameerr ?></td>
        </tr>

        <tr>
            <td>Date Of Birth</td>
            <td><input type="date" name="dob" value="<?php echo $row['dob']; ?>" > <?= $doberr ?> </td>
        </tr>

        <tr>
            <td>Gender</td>
            <td>
                <input type="radio" name="gender" value="Male" <?= ($row['gender'] == 'Male') ?  "checked" : "" ;  ?>>Male <br>
                <input type="radio" name="gender" value="Female" <?= ($row['gender'] == 'Female') ?  "checked" : "" ;  ?>>Female
                <?= $gendererr ?>
            </td>
        </tr>

        <tr>
            <td>Mobile</td>
            <td><input type="text" name="mobile" value="<?php echo $row['mobile']; ?>"> <?= $mobileerr ?> </td>
        </tr>

        <tr>
            <td>Description</td>
            <td><textarea name="description" cols="21" rows="5"
            ><?=$row['author_description'] ?></textarea> <?= $descriptionerr ?> </td>
        </tr>

        <tr>
        <td></td>
        <td><input class=" btn btn-primary" type="submit" name="submit" value="Update Author">
        </td>

        </tr>



    </table>
    </form>
    
</body>
</html>