<?php

session_start();


include "/opt/lampp/htdocs/LMS/header.php";
include "/opt/lampp/htdocs/LMS/connection.php";
include "/opt/lampp/htdocs/LMS/loginSessionValid.php";

$fname = $lname = $dob = $gender = $mobile = $description = "";

$fnameerr = $lnameerr = $doberr = $gendererr = $mobileerr = $descriptionerr = "";

if ($_SERVER['REQUEST_METHOD'] == "POST") {

    if (empty($_POST['fname'])) {
        $fnameerr = "First name is required";
    } else {
        $fname = test_input($_POST['fname']);
        if (!preg_match("/^[a-zA-Z ]*$/", $fname)) {
            $fnameerr = "Only whitespace and letters allowed in fname";
        } else {
            $fname = test_input($_POST['fname']);

        }
    }

    if (empty($_POST['lname'])) {
        $lnameerr = "Last name is required";
    } else {
        $lname = test_input($_POST['lname']);
        if (!preg_match("/^[a-zA-Z ]*$/", $lname)) {
            $lnameerr = "Only whitespace and letters allowed in lname";
        }
    }

    if (empty($_POST['gender'])) {
        $gendererr = "Gender is required";
    }
    else
    {
        $gender = test_input($_POST['gender']);

    }

    if (empty($_POST['mobile'])) {
        $mobileerr = "Mobile Number is required";
    } else {

        if (!preg_match('/^[0-9]{10}$/', $mobile) != 1) {
            $mobileerr = "Only digits are valid";
        } else {
            $mobile = test_input($_POST['mobile']);
        }
    }

    $fname = test_input($_POST['fname']);
    $lname = test_input($_POST['lname']);
    $dob = test_input($_POST['dob']);
    // $gender = test_input($_POST['gender']);
    $mobile = test_input($_POST['mobile']);
    $description = test_input($_POST['description']);

    if ($fnameerr || $lnameerr || $doberr || $gendererr || $mobileerr || $descriptionerr) {
        echo "Kindly enter the correct data";
    } else {
        $insertauthor = mysqli_query($connection, "INSERT INTO author (author_fname , author_lname , dob ,gender ,
    mobile , author_description) VALUES ('{$fname}','{$lname}','{$dob}','{$gender}','{$mobile}','{$description}')")
        or die(mysqli_error($connection));

        if ($insertauthor) {
            echo "<script> alert('New Author Added Successfully'); </script>";
            header("location:authordisplay.php");


        } else {
            echo "Insertion failed";
        }

    }

}

function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

// echo $fnameerr . "<br>". $lnameerr . "<br>" . $doberr . "<br>" . $gendererr . "<br>" . $mobileerr . "<br>" .$descriptionerr;

?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link
      rel="stylesheet"
      href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
    />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <a href="https://icons8.com/icon/132/search"></a>
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
    />
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    <title>Insert Author</title>

</head>
<body>


<div class="mt-5 text-">
    <h2>Add Author's Details</h2>
    <form method="post" class="mt-3 ">
    <table  class="d-flex justify-content-">
        <?php echo ""; ?>
        <tr>
            <td>First Name</td>
            <td><input type="text" name="fname" placeholder="Enter Author's First Name"><?php echo $fnameerr; ?></td>
        </tr>


        <tr>
            <td>Last Name</td>
            <td><input type="text" name="lname" placeholder="Enter Author's Last Name"><?php echo $lnameerr; ?></td>
        </tr>

        <tr>
            <td>Date Of Birth</td>
            <td><input type="date" name="dob" placeholder="Enter Author's DOB"><?php echo $doberr; ?></td>
        </tr>

        <tr>
            <td>Gender</td>
            <td><input type="radio" name="gender" value="Male">Male <br>
                <input type="radio" name="gender" value="Female">Female <br>
                <?php echo $gendererr; ?>
            </td>
        </tr>

        <tr>
            <td>Mobile</td>
            <td><input type="text" name="mobile" placeholder="Enter Author's Mobile Number"><?php echo $mobileerr; ?></td>
        </tr>

        <tr>
            <td>Description</td>
            <td><textarea name="description" id="description" cols="21" rows="5"></textarea></td><?php echo $descriptionerr; ?>
        </tr>

        <tr>
        <td></td>
        <td><input class=" btn btn-primary" type="submit" name="submit" value="Add Author">
        </td>

        </tr>



    </table>
    </form>
</div>



</body>
</html>